using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using HWO.Bacon.Models;
using HWO.Bacon.Messages;
using HWO.Bacon.Behaviours;
using HWO.Bacon.Extension;
using Newtonsoft.Json.Linq;

public class Bot
{
	public static void Main (string[] args)
	{
		string host = args[0];
		int port = int.Parse(args[1]);
		string botName = args[2];
		string botKey = args[3];
		string trackName = args.SafeGet(4);
		string password = args.SafeGet(5);
		string carCount = args.SafeGet(6);
		string create = args.SafeGet(7);

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using (TcpClient client = new TcpClient(host, port))
		{
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;
			
			SendMsg joinMsg = null;
			if (trackName != null && password != null && carCount != null)
			{
				bool shouldCreate = create != null;
				joinMsg = new JoinOrCreateRace(shouldCreate, botName, botKey, trackName, password, int.Parse(carCount));
				
				if (shouldCreate)
				{
					Console.WriteLine("Going to create race. To join this race, *open another terminal* and execute: ");
					Console.WriteLine("************************************************************");
					Console.WriteLine(String.Format(">> ./joinRace {0} {1} {2} {3} {4} {5}", host, port, "challenger_name", trackName, password, carCount));
					Console.WriteLine("************************************************************");
				}
				else
				{
					Console.WriteLine(String.Format("Joining Race {0}:{1}/{2} as {3}", host, port, trackName, botName));
				}
			}
			else
			{
				joinMsg = new Join(botName, botKey); 
				Console.WriteLine("Joining auto-generated race");
			}
			
			

			new Bot(botName, reader, writer, joinMsg);
		}
	}

	private StreamWriter writer;

	private TrackModel trackModel;

	private RaceOnInnerLane behaviour;

	private AdjustSpeed adjustSpeed;

	private string carName;

	Bot (string name, StreamReader reader, StreamWriter writer, SendMsg joinMsg)
	{
		this.writer = writer;
		carName = name;
		behaviour = new RaceOnInnerLane(carName);
		adjustSpeed = new AdjustSpeed(carName);
				
		send(joinMsg);
		
		string line;
		
		while ((line = reader.ReadLine()) != null)
		{
			BaconWrapper msg = JsonConvert.DeserializeObject<BaconWrapper>(line);			

			switch (msg.msgType)
			{
				case "carPositions":
					SendMsg switchLanesMsg = behaviour.Update(msg.data.ToString());
					if (switchLanesMsg != null)
					{
						send(switchLanesMsg);
					}
					else
					{
						float trottel = adjustSpeed.Update(msg.data.ToString(), msg.gameTick);
						send(new Throttle(trottel));
					}

					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":			
					var track = JObject.Parse(msg.data.ToString())["race"]["track"];					
					trackModel = JsonConvert.DeserializeObject<TrackModel>(track.ToString());
					trackModel.SetupForRace();
					behaviour.TrackModel = trackModel;
					adjustSpeed.trackModel = trackModel;
					send(new Ping());
					break;
				case "yourCar":
					Console.WriteLine("Our car " + msg.data);
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				case "crash":
					Console.WriteLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-Crashed-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
					send(new Ping());
					break;
				case "spawn":
					Console.WriteLine("Respawned");
					send(new Ping());
					break;
				case "lapFinished":
					Console.WriteLine("Lap finished: " + msg.data);
					send(new Ping());
					break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send (SendMsg msg)
	{
		writer.WriteLine(msg.ToJson());
	}
}






