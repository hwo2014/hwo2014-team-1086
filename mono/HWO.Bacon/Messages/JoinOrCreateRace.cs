﻿using System;
using Newtonsoft.Json.Linq;

namespace HWO.Bacon.Messages
{
	public class JoinOrCreateRace : SendMsg
	{
		string botName;
		string botKey;
		string trackName;
		string password;
		int carCount;
		bool create;

		public JoinOrCreateRace (bool create, string botName, string botKey, string trackName, string password, int carCount)
		{
			this.create = create;
			this.carCount = carCount;
			this.password = password;
			this.trackName = trackName;
			this.botKey = botKey;
			this.botName = botName;			
		}

		protected override object MsgData ()
		{
			JObject obj = new JObject();
			obj["botId"] = new JObject();
			obj["botId"]["name"] = botName;
			obj["botId"]["key"] = botKey;
			obj["trackName"] = trackName;
			obj["password"] = password;
			obj["carCount"] = carCount;
			return obj;
		}

		protected override string MsgType ()
		{
			return create ? "createRace" : "joinRace";
		}
	}
}

