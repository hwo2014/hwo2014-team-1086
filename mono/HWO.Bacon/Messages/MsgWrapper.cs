using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using HWO.Bacon;

namespace HWO.Bacon.Messages
{
	class MsgWrapper
	{
		public string msgType;

		public Object data;

		public MsgWrapper (string msgType, Object data)
		{
			this.msgType = msgType;
			this.data = data;
		}
	}
}
