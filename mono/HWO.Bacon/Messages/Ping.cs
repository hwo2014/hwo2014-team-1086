using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using HWO.Bacon;

namespace HWO.Bacon.Messages
{
	class Ping: SendMsg
	{
		protected override string MsgType ()
		{
			return "ping";
		}
	}
}
