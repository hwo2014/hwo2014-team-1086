﻿using System;

namespace HWO.Bacon.Messages
{
	public class SwitchLane : SendMsg
	{
		string direction;

		public SwitchLane (string direction)
		{
			this.direction = direction;
					
		}

		protected override object MsgData ()
		{
			return direction;
		}

		protected override string MsgType ()
		{
			return "switchLane";
		}

		public static SwitchLane Right = new SwitchLane("Right");

		public static SwitchLane Left = new SwitchLane("Left");
	}
}

