﻿using System;

namespace HWO.Bacon.Messages
{
	class BaconWrapper : MsgWrapper
	{
		public int gameTick;

		public BaconWrapper (string msgType, Object data, int gameTick) : base(msgType, data)
		{
			this.gameTick = gameTick;
		}
	}
}

