using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using HWO.Bacon;

namespace HWO.Bacon.Messages
{
	class Throttle: SendMsg
	{
		public double value;

		public Throttle (double value)
		{
			this.value = value;
		}

		protected override Object MsgData ()
		{
			return this.value;
		}

		protected override string MsgType ()
		{
			return "throttle";
		}
	}
}
