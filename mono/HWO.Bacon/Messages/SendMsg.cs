using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using HWO.Bacon;

namespace HWO.Bacon.Messages
{
	public abstract class SendMsg
	{
		public string ToJson ()
		{
			return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
		}

		protected virtual Object MsgData ()
		{
			return this;
		}

		protected abstract string MsgType ();
	}
}
