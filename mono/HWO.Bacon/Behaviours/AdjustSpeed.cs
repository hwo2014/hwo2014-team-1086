﻿using System;
using HWO.Bacon.Models;
using HWO.Bacon.Fuzz;
using Newtonsoft.Json.Linq;

namespace HWO.Bacon.Behaviours
{
	public class AdjustSpeed
	{
		public TrackModel trackModel;
		private string carName;

		private const string TOKEN_ID = "id";
		private const string TOKEN_NAME = "name";
		private const string TOKEN_PIECE_POSITION = "piecePosition";
		private const string TOKEN_PIECE_INDEX = "pieceIndex";
		private const string TOKEN_PIECE_DISTANCE = "inPieceDistance";
		private const string TOKEN_ANGLE = "angle";
		private const string TOKEN_LANE = "lane";
		private const string TOKEN_STARTLANE_INDEX = "startLaneIndex";
		

		private const float DEFAULT_SPEED = 0.6f;
		private const float MIN_SPEED = 0.1f;

		private float meterPerTick;
		private int lastPieceIndex = 0;
		private float lastPieceDistance = 0;

		public AdjustSpeed(string carName)
		{
			this.carName = carName;
		}

		public float Update(string data, int currentTick)
		{
			float throttle = DEFAULT_SPEED;

			JArray carPositions = JArray.Parse(data);

			foreach (var carPosition in carPositions)
			{
				if (IsMyCar(carPosition))
				{
					float distanceOnCurrentPiece = CurrentPieceDistance(carPosition);
					UpdateSpeedSinceLastTick(carPosition, distanceOnCurrentPiece);
					throttle = CalculateSpeed(carPosition, distanceOnCurrentPiece);

					Console.WriteLine("-==- THROTTLE "+throttle +" FOR MPT " + meterPerTick);
				}
			}
			return throttle;
		}

		private float CalculateSpeed(JToken carPositionToken, float distanceOnCurrentPiece)
		{
			int currentIndex = CurrentPieceIndex(carPositionToken);
			int indexPlus1 = NextPieceIndex(currentIndex);
			int indexPlus2 = NextPieceIndex(indexPlus1);

			Piece currentPiece = trackModel.pieces[currentIndex];			
			Piece piecePlus1 = trackModel.pieces[indexPlus1];
			Piece piecePlus2 = trackModel.pieces[indexPlus2];

			float currentDegree = GetMembershipDegreeForPiece(currentPiece, distanceOnCurrentPiece, true);
			float degreePlus1 = GetMembershipDegreeForPiece(piecePlus1, distanceOnCurrentPiece);
			float degreePlus2 = GetMembershipDegreeForPiece(piecePlus2, distanceOnCurrentPiece);


			float percentTravelled = (distanceOnCurrentPiece / currentPiece.LengthOfCurrentLane); 
			float shift = 0.8f;
			float speed = (currentDegree * shift * (1-percentTravelled)) + (degreePlus1 * (shift * percentTravelled) + degreePlus2 * 0.2f);

			return speed;
		}

		private float GetMembershipDegreeForPiece(Piece piece, float distanceOnPiece, bool isCurrent = false)
		{
			float membership = 0;
			if(IsInCurve(piece))
			{
				float extra = ExtraMembership(piece.Angle);

				if(isCurrent && IsLeavingCurve (piece, distanceOnPiece))
				{
					membership = ExitCurve(meterPerTick) + extra;
//					Console.WriteLine(" EXIT C "+membership +" mpt "+meterPerTick + " E" +extra);
				}
				else
				{
					membership = EnterCurve(meterPerTick) + extra;
//					Console.WriteLine(" ENTER C "+membership +" mpt "+meterPerTick);
				}
			}
			else
			{
				membership = GoingStraight(meterPerTick);
			}

			return membership;
		}

		private float ExtraMembership(float angle)
		{
			return (45 - Math.Abs(angle)) / 45;
		}

		private bool IsInCurve(Piece piece)
		{		
			return piece.Angle != 0;
		}
			
		private bool IsLeavingCurve(Piece currentPiece, float distanceOnPiece)
		{
			var nextPieceIndex = currentPiece.Index + 1 < trackModel.pieces.Count ? currentPiece.Index + 1 : 0;
			var nextPiece = trackModel.pieces[nextPieceIndex];
			return nextPiece.Angle == 0 && distanceOnPiece > currentPiece.LengthOfCurrentLane / 2f;
		}
		
		private float GoingStraight(float value)
		{
			return 1f;
		}

		private  float EnterCurve(float value)
		{
			return Fuzzy.ReverseGrade(value, 4f, 8f);
		}
		
		private float ExitCurve(float value)
		{
			return Fuzzy.ReverseGrade(value, 7f, 10f);
		}

		private float highestSpeed = 1;

		private void UpdateSpeedSinceLastTick(JToken carPositionToken, float distanceOnPiece)
		{
			int currentIndex = CurrentPieceIndex(carPositionToken);

			if(currentIndex != lastPieceIndex)
			{
				int prevIndex = PrevPieceIndex(currentIndex);
				Piece prevPiece = trackModel.pieces[prevIndex];

				float distanceOnLastPiece = prevPiece.LengthOfCurrentLane - lastPieceDistance;
				float mpt = distanceOnPiece + distanceOnLastPiece;

				if(mpt > 0 && mpt < (highestSpeed + 1)) // too lazy to fix the math
				{
					meterPerTick = mpt;
				}

				lastPieceIndex = currentIndex;
			}
			else
			{
				meterPerTick = distanceOnPiece - lastPieceDistance;
			}	

			highestSpeed = meterPerTick;

			lastPieceDistance = distanceOnPiece;
		}
			
		private int PrevPieceIndex(int currentPieceIndex)
		{
			return (currentPieceIndex - 1) < 0 ? trackModel.pieces.Count-1 : currentPieceIndex - 1;
		}

		private int NextPieceIndex(int currentPieceIndex)
		{
			return (currentPieceIndex + 1) >= trackModel.pieces.Count ? 0 : currentPieceIndex + 1;
		}

		private bool IsMyCar(JToken carPositionToken)
		{
			return carPositionToken[TOKEN_ID][TOKEN_NAME].ToString() == carName;
		}

		private int CurrentPieceIndex(JToken carPositionToken)
		{
			return (int)carPositionToken[TOKEN_PIECE_POSITION][TOKEN_PIECE_INDEX];
		}

		private float CurrentPieceDistance(JToken carPositionToken)
		{
			return (float)carPositionToken[TOKEN_PIECE_POSITION][TOKEN_PIECE_DISTANCE];
		}	

		private float CurrentAngle(JToken carPositionToken)
		{
			return (float)carPositionToken[TOKEN_ANGLE];
		}

//		{"msgType": "carPositions", "data": 
//		[
//			{
//				"id": {
//					"name": "Schumacher",
//					"color": "red"
//				},
//				"angle": 0.0,
//				"piecePosition": {
//					"pieceIndex": 0,
//					"inPieceDistance": 0.0,
//					"lane": {
//						"startLaneIndex": 0,
//						"endLaneIndex": 0
//					},
//					"lap": 0
//				}
//			},
//			{
//				"id": {
//					"name": "Rosberg",
//					"color": "blue"
//				},
//				"angle": 45.0,
//				"piecePosition": {
//					"pieceIndex": 0,
//					"inPieceDistance": 20.0,
//					"lane": {
//						"startLaneIndex": 1,
//						"endLaneIndex": 1
//					},
//					"lap": 0
//				}
//			}
//		], 
//		"gameId": "OIUHGERJWEOI", "gameTick": 0}
	}
}

