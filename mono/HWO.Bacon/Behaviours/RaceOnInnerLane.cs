﻿using System;
using HWO.Bacon.Models;
using HWO.Bacon.Messages;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.Linq;

namespace HWO.Bacon.Behaviours
{
	public class RaceOnInnerLane
	{
		public TrackModel TrackModel;

		string carName;
		
		KeyValuePair<int, SwitchDirection> scheduledSwitch = new KeyValuePair<int, SwitchDirection>(-1, SwitchDirection.NoSwitch);
		
		
		public RaceOnInnerLane (string carName)
		{
			this.carName = carName;
		}

		public SendMsg Update (string data)
		{
			SendMsg response = null;
			
			JArray carPositions = JArray.Parse(data);
			
			foreach (var carPosition in carPositions)
			{
				if (IsMyCar(carPosition))
				{
					var currentPieceIndex = CurrentPieceIndex(carPosition);					
					var nextPieceIndex = NextPieceIndex(currentPieceIndex);
					var currentLaneIndex = GetCurrentLaneIndex(carPosition);
					
					if (TrackModel.pieces[nextPieceIndex].Switch)
					{
						var desiredDirection = DetermineDirection(nextPieceIndex, currentLaneIndex);
						
						if (desiredDirection != SwitchDirection.NoSwitch && scheduledSwitch.Value != desiredDirection)
						{							
							var previousLane = GetEndLaneIndex(carPosition);
							var newLane = previousLane + (desiredDirection == SwitchDirection.Right ? 1 : -1);							
							response = desiredDirection == SwitchDirection.Right ? SwitchLane.Right : SwitchLane.Left;	
							scheduledSwitch = new KeyValuePair<int, SwitchDirection>(nextPieceIndex, desiredDirection);	
							
							Console.WriteLine(string.Format("Switch at: {0}, will switch lane to {1} (new lane {2}, previous lane {3})", nextPieceIndex, desiredDirection, newLane, previousLane));
						}										
					}
					else if (nextPieceIndex > scheduledSwitch.Key)
					{
						scheduledSwitch = new KeyValuePair<int, SwitchDirection>(-1, SwitchDirection.NoSwitch);	
					}
					
					SetLaneIndexForUpcomingPieces(carPosition);
										
					break;
				}									
			}
												
			return response;
		}

		bool IsMyCar (JToken carPositionToken)
		{
			return carPositionToken["id"]["name"].ToString() == carName;
		}

		int CurrentPieceIndex (JToken carPositionToken)
		{
			return (int)carPositionToken["piecePosition"]["pieceIndex"];
		}

		int NextPieceIndex (int currentPieceIndex)
		{
			return (currentPieceIndex + 1) >= TrackModel.pieces.Count ? 0 : currentPieceIndex + 1;
		}

		bool CarOnDesiredLane (bool isRight, JToken carPositionToken)
		{
			var currentLaneIndex = GetCurrentLaneIndex(carPositionToken);
			var currentLane = TrackModel.lanes.Find(lane => lane.Index == (int)currentLaneIndex);
						
			return currentLane == (isRight ? TrackModel.RightMostLane : TrackModel.LeftMostLane);
		}
		
		public SwitchDirection DetermineDirection (int nextPieceIndex, int currentLane)
		{
			float[] lengths = TrackModel.LengthsFromThisTillNextSwitchPiece[nextPieceIndex];
			int bestLane = currentLane;
			for (int i = 0; i < lengths.Length; i++)
			{
				if (lengths[i] < lengths[bestLane])
				{
					bestLane = i;	
				}
			}
			if (bestLane < currentLane)
			{
				return SwitchDirection.Left;
			}
			else if (bestLane > currentLane)
			{
				return SwitchDirection.Right;
			}
			return SwitchDirection.NoSwitch;
		}		
		
		JToken GetCurrentLane(JToken carPositionToken)
		{
			return carPositionToken["piecePosition"]["lane"];
		}
		
		int GetCurrentLaneIndex(JToken carPositionToken)
		{
			return (int)GetCurrentLane(carPositionToken)["startLaneIndex"];
		}
		
		int GetEndLaneIndex(JToken carPositionToken)
		{
			return (int)GetCurrentLane(carPositionToken)["endLaneIndex"];
		}		
	
		public void SetLaneIndexForUpcomingPieces (JToken carPositionToken, int amountOfPieces = 4)
		{
			var pieceIndex = CurrentPieceIndex(carPositionToken);
			var currentLaneIndex = GetCurrentLaneIndex(carPositionToken);
			for (int i = 0; i < amountOfPieces; i++)
			{
				var laneIndex = currentLaneIndex;
				if (scheduledSwitch.Value != SwitchDirection.NoSwitch && scheduledSwitch.Key < pieceIndex)
				{
					laneIndex += scheduledSwitch.Value == SwitchDirection.Left ? -1 : 1;
				}
				TrackModel.pieces[pieceIndex].CurrentLaneIndex = laneIndex;
			}
		}
	}
	
	public enum SwitchDirection
	{
		NoSwitch,
		Left,
		Right
	}
}

