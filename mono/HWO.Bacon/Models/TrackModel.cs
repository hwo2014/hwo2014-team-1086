using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities.LinqBridge;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using HWO.Bacon.Behaviours;

namespace HWO.Bacon.Models
{
	public class TrackModel
	{
		public string id;

		public string name;

		public List<Piece> pieces = new List<Piece>();

		public List<Lane> lanes = new List<Lane>();

		public Lane RightMostLane { get; private set; }

		public Lane LeftMostLane { get; private set; }

		public Dictionary<int, float[]> LengthsFromThisTillNextSwitchPiece = new Dictionary<int, float[]>();
		
		public void SetupForRace ()
		{
			ComputeLanesOrder();
			ComputeLengthOfLanesOnPieces();
		}

		private void ComputeLanesOrder ()
		{
			lanes.Sort((l1, l2) => (int)(l1.DistanceFromCenter - l2.DistanceFromCenter));
			LeftMostLane = lanes.First();
			RightMostLane = lanes.Last();
		}

		private void ComputeLengthOfLanesOnPieces ()
		{
			int switchIndex = -1;
			float[] lengthsTillFirstSwitch = null;
			float[] lengthsBetweenSwitches = new float[lanes.Count];
			for (int i = 0; i < pieces.Count; i++)
			{
				Piece piece = pieces[i];	
				piece.Index = i;
				piece.LengthForLane = new float[lanes.Count];
				for (int j = 0; j < lanes.Count; j++)
				{
					piece.Radians = ConvertToRadians(piece.Angle);
					if (piece.Radians != 0)
					{						
						piece.LengthForLane[j] = (piece.Radius - lanes[j].DistanceFromCenter * Math.Sign(piece.Radians)) * Math.Abs(piece.Radians);
					}
					else
					{						
						piece.LengthForLane[j] = piece.Length;
					}
				}
				
				if (piece.Switch)
				{
					if (lengthsTillFirstSwitch != null)
					{						
						LengthsFromThisTillNextSwitchPiece[switchIndex] = lengthsBetweenSwitches;
					}
					else
					{
						lengthsTillFirstSwitch = lengthsBetweenSwitches;
					}
					switchIndex = i;
					lengthsBetweenSwitches = new float[lanes.Count];
				}
				else
				{
					for (int j = 0; j < lanes.Count; j++)
					{
						lengthsBetweenSwitches[j] += piece.LengthForLane[j];
					}
				}

			}
			if (lengthsTillFirstSwitch != null)
			{
				for (int i = 0; i < lanes.Count; i++)
				{
					lengthsTillFirstSwitch[i] += lengthsBetweenSwitches[i];					
				}							
				LengthsFromThisTillNextSwitchPiece[switchIndex] = lengthsTillFirstSwitch;
			}
			
			
			Console.WriteLine(LengthsFromThisTillNextSwitchPiece.Select(kvp => kvp.Key + "[" + kvp.Value.Select(v => v.ToString()).Aggregate((v1,v2) => v1 + "|" + v2) + "]").Aggregate((kvp1, kvp2) => kvp1 + " | " + kvp2));
		}

		public float ConvertToRadians(float angle)
		{
			return (float)(Math.PI / 180) * angle;
		}

	}

	public class Lane
	{
		public int Index;

		public float DistanceFromCenter;
	}

	public class Piece
	{
		public int Index;
		
		public float Radius;

		public float Angle;
		
		public float Radians;

		public bool Switch;

		public float Length;
		
		public float LengthOfCurrentLane {
			get { 
				return LengthForLane != null ? LengthForLane[CurrentLaneIndex] : Length; 
			}
		}
		
		public float[] LengthForLane;
		
		public int CurrentLaneIndex;
	}
}

