﻿using System;

namespace  HWO.Bacon.Extension
{
	public static class Extensions
	{
		public static string SafeGet (this string[] source, int index)
		{
			if (index >= source.Length)
			{
				return null;
			}
			
			return source[index];
		}
	}
}

