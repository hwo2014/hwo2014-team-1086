## To create a race
`./createRace <host> <port> <trackName> <password> <carCount>`
Example:
`./createRace senna.helloworldopen.com 8091 france pwdownd 3`

## To join a race
`./joinRace <host> <port> <botName> <trackName> <password> <carCount>`
Example
`/joinRace senna.helloworldopen.com 8091 challenger_name france pwdownd 3`
Note: challenger names must be unique. Remember to change challenger
names for multiple challnegers. 
